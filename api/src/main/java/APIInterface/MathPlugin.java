/**
 * Name: Ong Ming Hang
 * ID: 19287368
 * Unit: Software Engineering Concepts
 * Tutor: David Cooper
 */

package APIInterface;

public interface MathPlugin
{
    /**
     * This is a plugin interface for a specific purpose, to allow mathematical library
     * to be loaded before the actual plugin attempts to use it (e.g. invokes a call to
     * factorial(x) even though it does not know if that method exist yet). The base
     * implementation is a one liner.
     * @param api the API
     */
    void start(Control api);
}
