/**
 * Name: Ong Ming Hang
 * ID: 19287368
 * Unit: Software Engineering Concepts
 * Tutor: David Cooper
 */

package APIInterface;

public interface Plugin
{
    /**
     * This is essentially the main() of the plugin, it is invoked by the core application
     * to pass in the API for the plugins to use. Each of the plugin that implements this
     * interface will use the API to suit their own purpose.
     *
     * Usage:
     * - Create a plugin base class in an existing or new subproject (ensure the dependency
     *   are fully sorted out) and implement this interface.
     * - Once implemented, begin using the given methods by the API.
     * - Run the core application and add in the plugin through the plugin manager.
     * - The core application will automatically run any of the added plugins that is in
     *   the list and invoke a call to start(Control api).
     * @param api the API
     */
    void start(Control api);
}