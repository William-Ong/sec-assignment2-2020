/**
 * Name: Ong Ming Hang
 * ID: 19287368
 * Unit: Software Engineering Concepts
 * Tutor: David Cooper
 */

package APIInterface;

public interface Control
{
    /**
     * This contains the methods that the plugin can use for their purpose,
     * instantiated with values from the core application. This class does not
     * know of the existence of plugins because all its purpose is to provide
     * functionality given by the core application in the plugins.
     */
    String fetchEquation();
    double fetchMinimumValue();
    double fetchMaximumValue();
    double fetchIncrementalValue();
    double fetchResult();
    void setResult(double result);
    void registerMathFunctionEvaluation(String expression);
}