/**
 * Name: Ong Ming Hang
 * ID: 19287368
 * Unit: Software Engineering Concepts
 * Tutor: David Cooper
 */

package APIInterface;

public interface MenuInterface
{
    void run();
    String menu();
}
