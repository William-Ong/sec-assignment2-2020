Name: Ong Ming Hang
ID: 19287368
Unit: Software Engineering Concepts

Purpose: A multi-level equation evaluator project that incorporates language integration to fulfill its purpose.

Usage:
- Run ./gradlew :app:run --console=plain in the root directory.
    - Notes:
        - :app:run - core application is in its own subproject, therefore normal ./gradlew run does not
          achieve anything substantial and throws dependency errors.
        - --console=plain - remove text produced by Gradle (e.g. progress indicator text) that provides
          an obstruction for the user.

Available plugins:
- When prompted to enter a plugin, use the following:
    - Progress.Indicator - Outputs a loading progress to the user
    - JavaImp.Indicator - Re-implements the above plugin using language integration
    - CSVOutput.WriteToCSV - Allows the program to output a CSV file in the 'app' subproject directory
- When prompted to enter a math plugin: use the following:
    - MathLib.ProvideMathFunctions - Allow the core application to import math functions so that the evaluator
                                     uses the imported static methods for more complex evaluation.
                                   - Currently supports fibonacci(x) and factorial(x).

Package Structure:
    CoreApp(:app) - the main subprojects that contains the core application
        - App.java - the main class that runs the program
        - AppMenu.java - the main menu
        - EquationController.java - the API container
        - Evaluator.java - the class that evaluates mathematical expressions
        - PluginManager.java - the plugin manager
        - UserIO.java - the user input/output manager
    APIInterface(:api) - this subproject that contains all of the interfaces that can be used
        - Control.java - the API interface
        - MathPlugin.java - the math plugin interface
        - MenuInterface.java - the menu interface
        - Plugin.java - the main plugin interface
    CSVOutput(:apacheio) - this subproject that handles writing values to a csv file
        - WriteToCSV.java - the csv writer plugin
    Progress(:progress) - the subproject that handles printing out a loading status to the user
        - Indicator.java - handles the loading prints
    MathLib(:math) - this subproject handles any math related functions
        - MathLibrary.java - contains mathematical static methods
        - ProvideMathFunctions.java - loads the math plugin to the core application
    JavaImp(:java_app) - re-implementation of the loading bar plugin using language integration
        - Indicator.java - invokes a call to use the C/C++ function
    C Library(:c_library) - contains the code for the C++ source code
        - Indicator.cpp - the C++ implementation of the progress indicator