/**
 * Name: Ong Ming Hang
 * ID: 19287368
 * Unit: Software Engineering Concepts
 * Tutor: David Cooper
 */

package CSVOutput;

import APIInterface.Control;
import APIInterface.Plugin;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class WriteToCSV implements Plugin
{
    // Pre-determined csv file
    private static final String CSV_FILE = "output.csv";

    @Override
    public void start(Control api) {
        try
        {
            // Opens a buffer writer to append more values to the output file or create one if it doesn't exist
            BufferedWriter writer = Files.newBufferedWriter(
                    Paths.get(CSV_FILE), StandardOpenOption.APPEND, StandardOpenOption.CREATE);
            // Creates a CSVPrinter object with the writer and default flag
            CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT);
            // Add the values into the writer
            csvPrinter.printRecord(api.fetchIncrementalValue(), api.fetchResult());
            csvPrinter.flush();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }
}