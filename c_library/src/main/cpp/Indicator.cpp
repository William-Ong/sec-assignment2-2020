/**
 * Name: Ong Ming Hang
 * ID: 19287368
 * Unit: Software Engineering Concepts
 * Tutor: David Cooper
 */

#include <jni.h>
#include <stdio.h>

// This construct is needed to make the C++ compiler generate C-compatible compiled code.
extern "C"
{
    /*public native void start(Control api);*/
    JNIEXPORT void JNICALL Java_JavaImp_Indicator_start(JNIEnv *env, jobject obj, jobject api)
    {
        // Get reference to Control class
        jclass controlClass = env->GetObjectClass(api);

        // Get reference to fetchIncrementalValue method
        jmethodID fetchIncrementalValue = env->GetMethodID(
            controlClass, "fetchIncrementalValue", "()D"
        );

        // Get reference to fetchIncrementalValue method
        jmethodID fetchMaximumValue = env->GetMethodID(
            controlClass, "fetchMaximumValue", "()D"
        );

        // Call api.fetchIncrementalValue() and return the double value
        double incVal = env->CallDoubleMethod(api, fetchIncrementalValue);

        // Call api.fetchMaximumValue() and return the double value
        double maxVal = env->CallDoubleMethod(api, fetchMaximumValue);

        // Calculate the percentage
        double percentage = incVal/maxVal * 100;

        // Print the loading indicator
        printf("Evaluating expression... %.2f%\n", percentage);
        if(incVal == maxVal)
        {
            printf("Evaluation status... Done!\n");
        }
        // Flush the output stream so it gets printed at runtime
        fflush(stdout);
    }
}