/**
 * Name: Ong Ming Hang
 * ID: 19287368
 * Unit: Software Engineering Concepts
 * Tutor: David Cooper
 */

package CoreApp;

import APIInterface.MenuInterface;

import java.util.InputMismatchException;

public class AppMenu implements MenuInterface
{
    private final UserIO io;
    private final PluginManager pm;
    private final Evaluator eval;

    public AppMenu(UserIO io, PluginManager pm, Evaluator eval)
    {
        this.io = io;
        this.pm = pm;
        this.eval = eval;
    }

    /**
     * A menu that gives user the option to choose from the prompts
     */
    @Override
    public void run()
    {
        boolean done = false;
        do
        {
            try
            {
                int choice = io.getInt(menu());
                switch(choice)
                {
                    case 1:
                        pm.run();
                        break;
                    case 2:
                        eval.run();
                        break;
                    case 3:
                        io.print("Exiting...");
                        done = true;
                        break;
                    default:
                        io.print("Invalid choice!");
                        break;
                }
            }
            catch(InputMismatchException e)
            {
                io.print("Invalid input detected! Error: " + e.getMessage());
                io.next();
            }
        }while(!done);
    }

    /**
     * Provides run() method with a menu prompt to print out to the user
     * @return the menu prompt
     */
    @Override
    public String menu()
    {
        return "=====Welcome to Equation Evaluator!=====\n" +
                "(1) Manage plugin\n" +
                "(2) Evaluate expression\n" +
                "(3) Exit\n" +
                "Enter an option: ";
    }
}
