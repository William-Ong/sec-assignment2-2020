/**
 * Name: Ong Ming Hang
 * ID: 19287368
 * Unit: Software Engineering Concepts
 * Tutor: David Cooper
 */

package CoreApp;

import APIInterface.Control;
import APIInterface.MathPlugin;
import APIInterface.MenuInterface;
import APIInterface.Plugin;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;

public class PluginManager implements MenuInterface
{
    private final UserIO io;
    private final List<Plugin> plugins;
    private final List<MathPlugin> mathPlugins;

    public PluginManager(UserIO io)
    {
        this.io = io;
        plugins = new ArrayList<>();
        mathPlugins = new ArrayList<>();
    }

    /**
     * A menu that gives user the option to choose from the prompts
     */
    @Override
    public void run()
    {
        boolean done = false;
        do
        {
            try
            {
                String pluginName = "";
                int choice = io.getInt(menu());
                switch(choice)
                {
                    case 1:
                        displayCurrentlyLoadedPlugins();
                        break;
                    case 2:
                        pluginName = io.getString("Enter name of plugin: ");
                        addNewPlugin(pluginName);
                        break;
                    case 3:
                        pluginName = io.getString("Enter name of math plugin: ");
                        addNewMathPlugin(pluginName);
                        break;
                    case 4:
                        io.print("Exiting...");
                        done = true;
                        break;
                    default:
                        io.print("Invalid choice!");
                        break;
                }
            }
            catch(InputMismatchException e)
            {
                io.print("Invalid input detected! Error: " + e.getMessage());
                io.next();
            }
        }while(!done);
    }

    /**
     * Provides run() method with a menu prompt to print out to the user
     * @return the menu prompt
     */
    @Override
    public String menu()
    {
        return "=====Plugin Manager=====\n" + "" +
                "(1) Display loaded plugins\n" +
                "(2) Add new plugin\n" +
                "(3) Add math plugin\n" +
                "(4) Exit\n" +
                "Enter option: ";
    }

    /**
     * Displays all the plugins to the user
     */
    public void displayCurrentlyLoadedPlugins()
    {
        // Display normal plugins
        io.print("=====Plugins=====");
        int count = 0;
        for(Plugin plugin : plugins)
        {
            io.print("[" + count + "]: " + plugin.toString());
            count++;
        }

        // Display math plugins
        io.print("=====Math Plugins=====");
        int mathCount = 0;
        for(MathPlugin mathPlugin : mathPlugins)
        {
            io.print("[" + mathCount + "]: " + mathPlugin.toString());
            mathCount++;
        }
    }

    /**
     * Add new plugin by giving it a string name, which searches using the dependencies given
     * @param pluginName the name of the plugin
     */
    public void addNewPlugin(String pluginName)
    {
        try
        {
            // Search for a class name from the given plugin name
            Class<?> pluginCls = Class.forName(pluginName);
            // Create an object of the valid plugin and instantiate its constructor
            Plugin pluginObj = (Plugin)pluginCls.getConstructor().newInstance();
            // Add the plugin object to the list
            plugins.add(pluginObj);
            // Show success prompt to the user
            io.print("Added plugin " + pluginName + " to the list!");
        }
        catch(ReflectiveOperationException | ClassCastException e)
        {
            io.print(pluginName + " is not a valid plugin! Error: " + e.getMessage());
        }
    }

    /**
     * Add new math plugin by giving it a string name, which searches using the dependencies given
     * @param pluginName the name of the math plugin
     */
    public void addNewMathPlugin(String pluginName)
    {
        try
        {
            // Search for a class name from the given plugin name
            Class<?> pluginCls = Class.forName(pluginName);
            // Create an object of the valid plugin and instantiate its constructor
            MathPlugin pluginObj = (MathPlugin) pluginCls.getConstructor().newInstance();
            // Add the plugin object to the list
            mathPlugins.add(pluginObj);
            // Show success prompt to the user
            io.print("Added math plugin " + pluginName + " to the list!");
        }
        catch(ReflectiveOperationException | ClassCastException e)
        {
            io.print(pluginName + " is not a valid plugin! Error: " + e.getMessage());
        }
    }

    /**
     * Execute all plugins in succession
     * @param api the container class provided to the plugins
     */
    public void executePlugin(Control api)
    {
        // For each plugin added, start it up
        for(Plugin plugin : plugins)
        {
            plugin.start(api);
        }
    }

    /**
     * Loads the math plugin into the program, for the context of this program, it is
     * using the Jython interpreter
     * @param api the container class provided to the plugins
     */
    public void loadMathPlugins(Control api)
    {
        // For each math plugin added, start it up
        for(MathPlugin mathPlugin : mathPlugins)
        {
            mathPlugin.start(api);
        }
    }
}
