/**
 * Name: Ong Ming Hang
 * ID: 19287368
 * Unit: Software Engineering Concepts
 * Tutor: David Cooper
 */
package CoreApp;

public class App
{
    public static void main(String[] args)
    {
        // Setup all related classes
        UserIO io = new UserIO();
        PluginManager pm = new PluginManager(io);
        Evaluator eval = new Evaluator(io, pm);

        // Run the application
        AppMenu appMenu = new AppMenu(io, pm, eval);
        appMenu.run();
    }
}
