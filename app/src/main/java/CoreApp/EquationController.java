/**
 * Name: Ong Ming Hang
 * ID: 19287368
 * Unit: Software Engineering Concepts
 * Tutor: David Cooper
 */

package CoreApp;

import APIInterface.Control;
import org.python.util.PythonInterpreter;

import java.util.ArrayList;
import java.util.List;

public class EquationController implements Control
{
    private final String equation;
    private final double minimumValue;
    private final double maximumValue;
    private final double incrementalValue;
    private final PythonInterpreter py;
    private final List<String> math_list;
    private double result;

    public EquationController(String equation, double minimumValue, double maximumValue, double incrementalValue, double result, PythonInterpreter py)
    {
        this.equation = equation;
        this.minimumValue = minimumValue;
        this.maximumValue = maximumValue;
        this.incrementalValue = incrementalValue;
        this.result = result;
        this.py = py;

        // Register implemented static mathematical method names for invoke
        math_list = new ArrayList<>();
        math_list.add("fibonacci");
        math_list.add("factorial");
    }

    @Override
    public String fetchEquation() {
        return equation;
    }

    @Override
    public double fetchMinimumValue() {
        return minimumValue;
    }

    @Override
    public double fetchMaximumValue() {
        return maximumValue;
    }

    @Override
    public double fetchIncrementalValue() {
        return incrementalValue;
    }

    @Override
    public double fetchResult() {
        return result;
    }

    @Override
    public void setResult(double result) {
        this.result = result;
    }

    /**
     * When invoked, registers the python interpreter with Java-implemented math methods
     * @param expression uses the given expression to identify any calls to static methods
     */
    @Override
    public void registerMathFunctionEvaluation(String expression)
    {
        for(String lib : math_list)
        {
            if(expression.toLowerCase().contains(lib.toLowerCase()))
            {
                py.exec("from MathLib.MathLibrary import " + lib);
            }
        }
    }
}
