/**
 * Name: Ong Ming Hang
 * ID: 19287368
 * Unit: Software Engineering Concepts
 * Tutor: David Cooper
 */

package CoreApp;

import java.util.*;

public class UserIO
{
    private final Scanner sc = new Scanner(System.in);

    /**
     * Get a double value using standard input
     * @param prompt the prompt for the user
     * @return the double value
     */
    public double getDouble(String prompt)
    {
        inputPrint(prompt);
        return sc.nextDouble();
    }

    /**
     * Get an integer value using standard input
     * @param prompt the prompt for the user
     * @return the integer value
     */
    public int getInt(String prompt)
    {
        inputPrint(prompt);
        return sc.nextInt();
    }

    /**
     * Get a string value using standard input
     * @param prompt the prompt for the user
     * @return the string value
     */
    public String getString(String prompt)
    {
        inputPrint(prompt);
        sc.nextLine(); // Flush the input stream
        return sc.nextLine();
    }

    /**
     * Flush the standard input when entering strings
     */
    public void next()
    {
        sc.next();
    }

    /**
     * Gives a prompt that does not contain a new line, for input usages
     * @param prompt the prompt to be printed
     */
    public void inputPrint(String prompt)
    {
        System.out.print(prompt);
    }

    /**
     * Gives a prompt that contain a new line
     * @param prompt the prompt to be printed
     */
    public void print(String prompt)
    {
        System.out.println(prompt);
    }
}
