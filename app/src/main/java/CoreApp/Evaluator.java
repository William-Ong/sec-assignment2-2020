/**
 * Name: Ong Ming Hang
 * ID: 19287368
 * Unit: Software Engineering Concepts
 * Tutor: David Cooper
 */

package CoreApp;

import org.python.core.*;
import org.python.util.*;
import APIInterface.*;

import java.util.InputMismatchException;

public class Evaluator implements MenuInterface
{
    private final UserIO io;
    private final PluginManager pm;
    private final PythonInterpreter py;
    private String expression;
    private double minX;
    private double maxX;
    private double incX;

    public Evaluator(UserIO io, PluginManager pm)
    {
        this.io = io;
        this.pm = pm;
        py = new PythonInterpreter();
        expression = "";
        minX = 0;
        maxX = 10;
        incX = 1;
    }

    /**
     * Value reset, in the case of result-bending errors.
     */
    public void reset()
    {
        expression = "";
        minX = 0;
        maxX = 10;
        incX = 1;
    }

    /**
     * A menu that gives user the option to choose from the prompts
     */
    @Override
    public void run()
    {
        boolean done = false;
        do
        {
            try
            {
                int choice = io.getInt(menu());
                switch(choice)
                {
                    case 1:
                        expression = io.getString("Enter mathematical expression: ");
                        break;
                    case 2:
                        minX = io.getDouble("Enter minimum value: ");
                        break;
                    case 3:
                        maxX = io.getDouble("Enter maximum value: ");
                        break;
                    case 4:
                        incX = io.getDouble("Enter increment value: ");
                        break;
                    case 5:
                        io.print(showCurrentValue());
                        break;
                    case 6:
                        if(!expression.equals(""))
                            evaluate();
                        else
                            io.print("Empty expression detected! Please enter expression.");
                        break;
                    case 7:
                        io.print("Exiting...");
                        done = true;
                        break;
                    default:
                        io.print("Invalid choice!");
                        break;
                }
            }
            catch(InputMismatchException e)
            {
                io.print("Invalid input detected! Error: " + e.getMessage() + "\nResetting evaluator to default state...");
                reset();
                io.next();
            }
        }while(!done);
    }

    /**
     * Provides run() method with a menu prompt to print out to the user
     * @return the menu prompt
     */
    @Override
    public String menu()
    {
        return "=====Evaluating Expression=====\n" +
                "(1) Enter expression\n" +
                "(2) Change minimum value\n" +
                "(3) Change maximum value\n" +
                "(4) Change increment\n" +
                "(5) Check values\n" +
                "(6) Submit\n" +
                "(7) Exit\n" +
                "Enter option: ";
    }

    /**
     * Evaluate a given expression after it has been chosen
     */
    private void evaluate()
    {
        Control api;

        for(double x = minX; x <= maxX; x += incX)
        {
            // Careful not to overwrite the class field.
            String temp_expression = expression;
            // Replace all instance of 'x' in string with values of x.
            temp_expression = temp_expression.replace("x", String.valueOf(x));

            double result = 0;
            try
            {
                // Creates the API for plugin to use on each iteration
                api = new EquationController(temp_expression, minX, maxX, x, result, py);

                // Load math library before calculation (if there is any), to prevent exception from being thrown
                pm.loadMathPlugins(api);

                // Invoke a call to the interpreter to calculate the expression
                result = ((PyFloat) py.eval("float(" + temp_expression + ")")).getValue();

                // Set the new result value (Note: This is required because it was a default value before calculation)
                api.setResult(result);

                // Start up the plugin
                pm.executePlugin(api);
            }
            catch(PyException e)
            {
                io.print("Invalid expression detected! Please enter new expression and check values.");
                io.print(showCurrentValue());
                break; // Prevents the terminal from printing multiple errors.
            }
        }
    }

    /**
     * Show the user the current inputted values, just in case a typo is made by the user.
     * @return the string containing informations about the current value
     */
    private String showCurrentValue()
    {
        return String.format("=====Values=====\n" +
                        "Expression: %s\n" +
                        "Minimum: %.2f (Note: Beware of zero-division!)\n" +
                        "Maximum: %.2f\n" +
                        "Increment: %.2f\n",
                expression, minX, maxX, incX);
    }
}