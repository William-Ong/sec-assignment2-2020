/**
 * Name: Ong Ming Hang
 * ID: 19287368
 * Unit: Software Engineering Concepts
 * Tutor: David Cooper
 */

package MathLib;

import APIInterface.Control;
import APIInterface.MathPlugin;

public class ProvideMathFunctions implements MathPlugin
{
    @Override
    public void start(Control api) {
        // Invokes a call to the api to load in the math library
        api.registerMathFunctionEvaluation(api.fetchEquation());
    }
}
