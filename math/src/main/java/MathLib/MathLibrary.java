/**
 * Name: Ong Ming Hang
 * ID: 19287368
 * Unit: Software Engineering Concepts
 * Tutor: David Cooper
 */

package MathLib;

public class MathLibrary
{
    /**
     * Given a value, return the result of the fibonacci algorithm. Takes in non-negative integers.
     * @param variable the value
     * @return the result
     */
    public static double fibonacci(double variable)
    {
        double n = Math.max(0, variable);

        if(n <= 1)
        {
            return Math.max(0, n);
        }
        return fibonacci(n - 1) + fibonacci(n - 2);
    }

    /**
     * Given a value, return the result of the factorial algorithm. Takes in non-negative integers.
     * @param variable the value
     * @return the result
     */
    public static double factorial(double variable)
    {
        double n = Math.max(0, variable);

        if(n == 0)
            return 1;
        else
            return Math.max(0, n * factorial(n - 1));
    }
}
