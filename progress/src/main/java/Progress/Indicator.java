/**
 * Name: Ong Ming Hang
 * ID: 19287368
 * Unit: Software Engineering Concepts
 * Tutor: David Cooper
 */

package Progress;

import APIInterface.Control;
import APIInterface.Plugin;

import java.text.DecimalFormat;

public class Indicator implements Plugin
{
    DecimalFormat dc2 = new DecimalFormat("#.##");

    @Override
    public void start(Control api) {
        // Current incremental value
        double i = api.fetchIncrementalValue();
        // The current percentage progress
        double percentage = i/api.fetchMaximumValue() * 100;
        // Constant printing until it has finished
        System.out.println("Evaluating expression... " + dc2.format(percentage) + "%");
        // One the evaluation has finished, output a status
        if(i == api.fetchMaximumValue())
            System.out.println("Evaluation status... Done!");
    }
}
