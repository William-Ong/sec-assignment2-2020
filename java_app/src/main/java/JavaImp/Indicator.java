/**
 * Name: Ong Ming Hang
 * ID: 19287368
 * Unit: Software Engineering Concepts
 * Tutor: David Cooper
 */

package JavaImp;

import APIInterface.Control;
import APIInterface.Plugin;

public class Indicator implements Plugin
{
    static
    {
        System.loadLibrary("mynativelibrary");
    }

    @Override
    public native void start(Control api);
}
